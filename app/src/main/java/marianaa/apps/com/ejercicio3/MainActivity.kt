package marianaa.apps.com.ejercicio3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var number1 : EditText
    lateinit var number2 : EditText
    lateinit var compare : Button
    lateinit var result : TextView
    var winner : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        number1 = findViewById(R.id.et_number1)
        number2 = findViewById(R.id.et_number2)
        compare = findViewById(R.id.btn_compare)
        result = findViewById(R.id.tv_number)
        compare.setOnClickListener{
            Toast.makeText(this, "Comparando", Toast.LENGTH_SHORT).show()
            compareNumbers()
        }
    }
    fun compareNumbers(){
        var num1 : Int = 0
        var num2 : Int = 0

        num1 = number1.text.toString().toInt()
        num2 = number2.text.toString().toInt()

        if(num1>num2){
            winner = num1
        } else {
            winner = num2
        }
        result.setText(winner.toString())
    }
}